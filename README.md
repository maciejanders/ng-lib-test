# NgLibTestApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.

It's based on this [tutorial](https://medium.com/@nikolasleblanc/building-an-angular-4-component-library-with-the-angular-cli-and-ng-packagr-53b2ade0701e)

## To package

`npm run packagr`

## To run test app

`npm install`

`ng serve`
